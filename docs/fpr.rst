
Example Usage of the *Field\_Protocol\_Reader.py* Methods
=========================================================

Setup
-----

Import modules and change current working directory to setup
environment:

.. code:: python

    #!/usr/bin/env python
    # -*- coding: utf-8 -*-from __future__ import print_function
    from mni2017.field_protocol_reader import field_data, plot_field, create_multiindex_df, create_reasonable_multiindex_df, plot_from_reasonable_df
    import os
    
    # change current working directory to folder with field protocol:
    path = '/media/thomas/NAS_Data/2017_MNI_campaign/field_data/MULTIPLY'
    os.chdir(path)
    
    # initialize field_data class object, load field data in 'MULTIPLY.xlsx
    field = field_data(filename='MULTIPLY.xlsx')


UPDATE
------

added *create\_reasonable\_multiindex\_df* to facilitate working with
the actual data [STRIKEOUT:as default option when initializing the class
object.]

[STRIKEOUT:One may set the option *create\_df* *False* if additional
.xlsx-files should be added to the class object as this only works with
the old procedure.]

[STRIKEOUT:The dataframe can then afterwards be created via
*create\_reasonable\_multiindex\_df(field.data.items())*.]

.. code:: python

    field_df = create_reasonable_multiindex_df(field.data.items())
    field_df




.. raw:: html

    <div>
    <style>
        .dataframe thead tr:only-child th {
            text-align: right;
        }
    
        .dataframe thead th {
            text-align: left;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr>
          <th></th>
          <th colspan="10" halign="left">301 (WT) high</th>
          <th>...</th>
          <th colspan="10" halign="left">542 (WT) medium</th>
        </tr>
        <tr>
          <th></th>
          <th>BBCH</th>
          <th>Chlorophyll measurement 1 [avarage of 10 SPAD Units]</th>
          <th>Chlorophyll measurement 2 [avarage of 10 SPAD Units]</th>
          <th>Chlorophyll measurement 3 [avarage of 10 SPAD Units]</th>
          <th>Chlorophyll measurement 4 [avarage of 10 SPAD Units]</th>
          <th>Chlorophyll measurement 5 [avarage of 10 SPAD Units]</th>
          <th>Comments</th>
          <th>Dry biomass fruit [g]</th>
          <th>Dry biomass leaf [g]</th>
          <th>Dry biomass stem [g]</th>
          <th>...</th>
          <th>Water content total [%]</th>
          <th>Water loss fruit [g]</th>
          <th>Water loss leaf [g]</th>
          <th>Water loss stem [g]</th>
          <th>Water loss total [g]</th>
          <th>Weather</th>
          <th>Wet biomass fruit [g]</th>
          <th>Wet biomass leaf [g]</th>
          <th>Wet biomass stem [g]</th>
          <th>Wet biomass total [g]</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>2017-03-24</th>
          <td>24.0</td>
          <td>49.2</td>
          <td>53.1</td>
          <td>48.9</td>
          <td>56.1</td>
          <td>47.4</td>
          <td>NaN</td>
          <td>0.0</td>
          <td>4.22</td>
          <td>0.94</td>
          <td>...</td>
          <td>75.56</td>
          <td>0.0</td>
          <td>9.12</td>
          <td>0.99</td>
          <td>10.11</td>
          <td>cloudy</td>
          <td>0.0</td>
          <td>11.96</td>
          <td>1.42</td>
          <td>13.38</td>
        </tr>
        <tr>
          <th>2017-03-28</th>
          <td>24.0</td>
          <td>44.2</td>
          <td>42.1</td>
          <td>47.8</td>
          <td>38.9</td>
          <td>53.2</td>
          <td>NaN</td>
          <td>0.0</td>
          <td>6.69</td>
          <td>7.06</td>
          <td>...</td>
          <td>80.71</td>
          <td>0.0</td>
          <td>10.21</td>
          <td>2.26</td>
          <td>12.47</td>
          <td>sunny</td>
          <td>0.0</td>
          <td>12.78</td>
          <td>2.67</td>
          <td>15.45</td>
        </tr>
        <tr>
          <th>2017-04-05</th>
          <td>28.0</td>
          <td>56.4</td>
          <td>57.5</td>
          <td>55.6</td>
          <td>51.3</td>
          <td>52.1</td>
          <td>NaN</td>
          <td>0.0</td>
          <td>6.88</td>
          <td>3.14</td>
          <td>...</td>
          <td>84.76</td>
          <td>0.0</td>
          <td>22.24</td>
          <td>6.19</td>
          <td>28.43</td>
          <td>cloudy</td>
          <td>0.0</td>
          <td>26.45</td>
          <td>7.09</td>
          <td>33.54</td>
        </tr>
        <tr>
          <th>2017-04-10</th>
          <td>29.0</td>
          <td>50.1</td>
          <td>55.9</td>
          <td>45.5</td>
          <td>56.2</td>
          <td>54.6</td>
          <td>NaN</td>
          <td>0.0</td>
          <td>18.51</td>
          <td>9.71</td>
          <td>...</td>
          <td>84.01</td>
          <td>0.0</td>
          <td>7.90</td>
          <td>1.98</td>
          <td>9.88</td>
          <td>sunny</td>
          <td>0.0</td>
          <td>9.52</td>
          <td>2.24</td>
          <td>11.76</td>
        </tr>
        <tr>
          <th>2017-04-21</th>
          <td>31.0</td>
          <td>40.9</td>
          <td>53.8</td>
          <td>44.1</td>
          <td>56.2</td>
          <td>48.8</td>
          <td>NaN</td>
          <td>0.0</td>
          <td>18.75</td>
          <td>13.54</td>
          <td>...</td>
          <td>81.70</td>
          <td>0.0</td>
          <td>18.06</td>
          <td>9.85</td>
          <td>27.91</td>
          <td>cloudy</td>
          <td>0.0</td>
          <td>22.42</td>
          <td>11.74</td>
          <td>34.16</td>
        </tr>
        <tr>
          <th>2017-05-02</th>
          <td>31.0</td>
          <td>47.7</td>
          <td>46.7</td>
          <td>42.4</td>
          <td>37.2</td>
          <td>47.5</td>
          <td>NaN</td>
          <td>0.0</td>
          <td>13.13</td>
          <td>11.48</td>
          <td>...</td>
          <td>82.55</td>
          <td>0.0</td>
          <td>43.87</td>
          <td>23.58</td>
          <td>67.45</td>
          <td>cloudy</td>
          <td>0.0</td>
          <td>53.85</td>
          <td>27.86</td>
          <td>81.71</td>
        </tr>
      </tbody>
    </table>
    <p>6 rows × 705 columns</p>
    </div>



.. code:: python

    list(field_df['301 (WT) high'])  # List all available values




.. parsed-literal::

    ['BBCH',
     'Chlorophyll measurement 1 [avarage of 10 SPAD Units]',
     'Chlorophyll measurement 2 [avarage of 10 SPAD Units]',
     'Chlorophyll measurement 3 [avarage of 10 SPAD Units]',
     'Chlorophyll measurement 4 [avarage of 10 SPAD Units]',
     'Chlorophyll measurement 5 [avarage of 10 SPAD Units]',
     'Comments',
     'Dry biomass fruit [g]',
     'Dry biomass leaf [g]',
     'Dry biomass stem [g]',
     'Dry biomass total [g]',
     'Endtime field',
     'Estimated share of brown leaf area [%] lower layer',
     'Estimated share of brown leaf area [%] upper layer',
     'Field overview picture from',
     'Field overview picture to',
     'Height [cm]',
     'LAI file name',
     'LAI measurement [cm²]',
     'LAT-WGS84',
     'LON-WGS84',
     'Name',
     'Plant count',
     'Plants',
     'Plants per meter',
     'Row orientation [°]',
     'Row spacing [cm]',
     'Sky cover [x/8]',
     'Soil 5cm',
     'Soil surface',
     'Starttime ESU',
     'Starttime field',
     'UTM Zone 32 Hochwert',
     'UTM Zone 32 Rechtswert',
     'Water content fruit [%]',
     'Water content leaf [%]',
     'Water content stem [%]',
     'Water content total [%]',
     'Water loss fruit [g]',
     'Water loss leaf [g]',
     'Water loss stem [g]',
     'Water loss total [g]',
     'Weather',
     'Wet biomass fruit [g]',
     'Wet biomass leaf [g]',
     'Wet biomass stem [g]',
     'Wet biomass total [g]']



.. code:: python

    field_df['301 (WT) high'][['BBCH', 'Height [cm]']]  # Filter the data




.. raw:: html

    <div>
    <style>
        .dataframe thead tr:only-child th {
            text-align: right;
        }
    
        .dataframe thead th {
            text-align: left;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>BBCH</th>
          <th>Height [cm]</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>2017-03-24</th>
          <td>24.0</td>
          <td>12.0</td>
        </tr>
        <tr>
          <th>2017-03-28</th>
          <td>24.0</td>
          <td>10.0</td>
        </tr>
        <tr>
          <th>2017-04-05</th>
          <td>28.0</td>
          <td>28.0</td>
        </tr>
        <tr>
          <th>2017-04-10</th>
          <td>29.0</td>
          <td>32.0</td>
        </tr>
        <tr>
          <th>2017-04-21</th>
          <td>31.0</td>
          <td>37.0</td>
        </tr>
        <tr>
          <th>2017-05-02</th>
          <td>31.0</td>
          <td>39.0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: python

    # Plot 3 values for field '301 (WT) high':
    values = ['BBCH','Height [cm]', 'Chlorophyll measurement 1 [avarage of 10 SPAD Units]']
    field_id = '301 (WT) high'
    
    plot_from_reasonable_df(field_df, '301 (WT) high', values)




.. image:: output_6_0.png


.. code:: python

    import matplotlib.pyplot as plt
    import pandas as pd
    
    # plot relative development of plant compartments
    y = field_df[field_id][['Wet biomass total [g]']]
    x = field_df[field_id][[ 'Wet biomass fruit [g]','Wet biomass leaf [g]','Wet biomass stem [g]']]
    
    # make y a Series
    y = y.iloc[:,0]
    type(y)
    # relative biomass:
    x_new = x.div(y, axis='index')
    
    # rename columns and plot
    x_new.columns = ['wet biomass fruit [%]','wet biomass leaf [%]','wet biomass stem [%]']
    x_new.plot(figsize=(10,8), fontsize = 14)
    plt.legend(fontsize=14)
    plt.grid()
    plt.show()



.. image:: output_7_0.png


