==============================
MNI2017
==============================

Scripts to process data from MNI2017 campaign.


Getting started
===============

Install package via:
----------------------

.. code-block:: bash

    cd /path/to/mni2017/repository/ # get to directory which contains setup.py
    python setup.py install

and use the command line tools or python methods as described in following sections.


TODO
======

+ testing not implemented
+ ...


Contents
========

.. toctree::
   :maxdepth: 2

   LAI extraction <lai>
   Field Protocol Reader <fpr>
   License <license>
   Authors <authors>
   Changelog <changes>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
