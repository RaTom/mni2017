
Aggregate informations from field protocols
============================================

TBD




Example usage of command line tool:
-----------------------------------------

.. code-block:: bash

    cd /path/to/field/protocols  # get to directory containing field protocols

    ...TBD

Example usage of python module:
-----------------------------------------

.. code-block:: python

    from mni2017 import ...

    TBD
