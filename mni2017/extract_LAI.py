#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This script helps extracting LAI values from txt files.
"""
from __future__ import division, print_function, absolute_import
import glob
import os
import sys
import pandas as pd
# import numpy as np
from scipy.spatial import cKDTree
import datetime

from mni2017 import __version__

__author__ = "Thomas Ramsauer"
__copyright__ = "Thomas Ramsauer"
__license__ = "gpl3"

# try:
#     input = raw_input
# except NameError:
#     pass


class LAIdata(object):

    """
    TBD
    """
    field_IDs = {"301_high": (48.311267495155334, 11.690093586221337),
                 "301_low": (48.309003710746765, 11.690081600099802),
                 "301_medium": (48.309491202235222, 11.688159210607409),
                 "319_high": (48.286525961011648, 11.661576256155968),
                 "319_low": (48.285751305520535, 11.660590711981058),
                 "319_medium": (48.285249480977654, 11.661955788731575),
                 "508_high": (48.25078678317368, 11.719030933454633),
                 "508_low": (48.251360692083836, 11.71728297136724),
                 "508_medium": (48.249399159103632, 11.719873482361436),
                 "515_high": (48.278193846344948, 11.71495464630425),
                 "515_low": (48.28066005371511, 11.712945336475968),
                 "515_medium": (48.28431548550725, 11.712197419255972),
                 "542_high": (48.284000912681222, 11.738448617979884),
                 "542_low": (48.284318000078201, 11.73987815156579),
                 "542_medium": (48.282870277762413, 11.739336932078004),
                 # "ESU_1": (48.248940, 11.717694),
                 # "ESU_2": (48.248851, 11.717708),
                 # "ESU_3": (48.248761, 11.717722),
                 # "ESU_4": (48.248949, 11.717828),
                 # "ESU_5": (48.248860, 11.717842),
                 # "ESU_6": (48.248770, 11.717856),
                 # "ESU_7": (48.248959, 11.717962),
                 # "ESU_8": (48.248869, 11.717976),
                 # "ESU_9": (48.248780, 11.717990)
                 "EnMap1": (48.248780, 11.717990),
                 "EnMap2": (48.285201, 11.713223)}

# EnMAP coords generated from pixel_geometrie.xlsx and transformed
# with easy online converter http://www.zonums.com/online/coords/cotrans.php
# 701742.063, 5347540.26
# 701743.448767971, 5347530.35648309
# 701744.834535943, 5347520.45296619
# 701751.966516907, 5347541.64576797
# 701753.352284879, 5347531.74225106
# 701754.73805285, 5347521.83873416
# 701761.870033814, 5347543.03153594
# 701763.255801786, 5347533.12801904
# 701764.641569757, 5347523.22450213

    def __init__(self, filename='data.csv', desired_col_names='standard'):
        self.filename = filename
        if desired_col_names == 'standard':
            self.desired_col_names = ['LAI_FILE', 'DATE', 'ESU_ID',
                                      'Field_N', 'LAI', 'SEL',
                                      'GPSLAT', 'GPSLONG', 'Comments']
        else:
            self.desired_col_names = desired_col_names
        data = self._setup_df()
        self.data = data

    def __str__(self):
        return "{}".format(self.data)

    def _setup_df(self):
        # example = glob.glob('*.TXT')[0]
        # with open(example, 'r') as f:
        #     lines = f.readlines()
        #     col_names = [line.split('\t')[0] for line in lines]
        # desired_col_names = [col_names[i] for i in [0, 2, 15, 16]]
        data = self._load_if_exists(self.desired_col_names)
        return data

    def _load_if_exists(self, desired_col_names=None):
        if len(glob.glob(self.filename)) == 1:
            data = pd.read_csv(self.filename, sep=';', index_col=0)
            try:
                data['DATE'] = pd.to_datetime(data['DATE'])
            except:
                print('could not convert input column \'DATE\''
                      'to datetime objects')
            print('\nloaded file "{}".'.format(self.filename))
        else:
            data = pd.DataFrame(columns=self.desired_col_names)
            print('\ncreated empty dataframe. no file (\'{}\') found..'
                  .format(self.filename))
        return data

    def save_LAIdata(self, filename=None):
        if filename is None:
            filename = self.filename
        self.data.to_csv(path_or_buf=filename, sep=';')
        print('Saved file "{}".\n'.format(filename))

    def extract_LAI(self, pattern="*.TXT", r=True):
        """
        pattern: string
            string describing files to be exploited. wildcards possible.
        recursive: boolean
            should directories be searched recursively
        """
        if self.data is not None and not isinstance(self.data, object):
            raise ValueError('data is neither None nor an object.')
        if r and sys.version_info[0] >= 3:
            pattern = '**/' + pattern
            listfile = glob.glob(pattern, recursive=r)
        elif r and sys.version_info[0] < 3:
            listfile = glob.glob(pattern)
            print('recursive file search from python 3.5 onwards.')
        else:
            listfile = glob.glob(pattern)
        print('\n--------------------------------------------------\n'
              'found {} LAI .TXT-file(s).'.format(len(listfile)))
        print('Extracting rows with following labels:\n{}\n'
              .format(self.desired_col_names))

        if self.data is not None:
            count = len(self.data)  # row length ?
        else:
            count = -1
        skipped_due_to_zero = []
        # print('len(self.data) = {}'.format(len(self.data)))
        for singlefile in listfile:
            count += 1
            d = {}
            with open(singlefile, 'r') as f:
                print('reading {}...'.format(singlefile))
                # read line by line
                for line in f:
                    line = line.strip()  # remove leading and trailing whitesp.
                    if not line:  # skip empty lines
                        continue
                    col_name = line.split('\t')[0]
                    if col_name in self.desired_col_names:
                        value = line.split('\t')[1]
                        d.update({col_name: value})
                d.update({'Field_N': str(d['LAI_FILE'])[:3]})

                # LAT LON:
                try:
                    lat = d['GPSLAT']
                    lon = d['GPSLONG']
                    POI = (lat, lon)
                    if (POI[0] is not 0 and POI[0] is not None and
                            POI[1] is not 0 and POI[1] is not None):
                        # get coordinates from class variable 'field_IDs'
                        combined_LAT_LON = list(LAIdata.field_IDs.values())
                        # build KDtree
                        mytree = cKDTree(combined_LAT_LON)
                        # query tree with POI
                        dist, indexes = mytree.query(POI)
                        # extract nearest field Coordinates
                        field_coords = tuple(combined_LAT_LON[indexes])
                        # get the key (hence ESU ID) from class variable by
                        # passing the index of found field_coords.
                        try:
                            ESU_ID = (LAIdata.field_IDs.keys()
                                      [LAIdata.field_IDs.values()
                                       .index(field_coords)])
                        except(AttributeError):  # for python 3:
                            ESU_ID = (list(LAIdata.field_IDs.keys())
                                      [list(LAIdata.field_IDs.values())
                                       .index(field_coords)])
                    d.update({'ESU_ID': ESU_ID})
                except KeyError:
                    print('  [WARNING] No GPSdata recorded!'
                          ' Using filename to create ESU_ID')
                    d.update({'ESU_ID': str(d['LAI_FILE'])[:3] + "_???"})
                if (d['ESU_ID'][:3] != 'ESU'):
                    if d['ESU_ID'][:3] != d['Field_N']:
                        print(d['ESU_ID'][:3], d['Field_N'])
                        d.update({'Comments': 'field number from filename does'
                                              ' not match coordinates'})
                try:
                    if d['Comments'] is not None:
                        print(d['Comments'])
                except(KeyError):
                    d.update({'Comments': ' '})
            print(d['LAI'])
            try:
                if d['LAI'] == '0.000':
                    skipped_due_to_zero.append(d['DATE'])
                    continue
            except:
                print('d[\'LAI\'] cannot be converted to in')
                # append new data after converting to Dataframe
            self.data = self.data.append(pd.DataFrame.from_records([d]))
            del d

        l = len(self.data)
        self.data['DATE'] = pd.to_datetime(self.data['DATE'])
        unique = self.data.drop_duplicates(subset=['DATE'])
        desired = unique[self.desired_col_names]
        sort = (desired.sort_values(['ESU_ID', 'DATE']))
        reset = sort.reset_index(drop=True)
        self.data = reset
        print('\n--------------------------------------------------\n'
              'removed {} rows due to duplicate date.\n{} rows remaining.\n'
              'dropped {} lines due to LAI == 0 for dates:\n'
              .format(l - len(self.data), len(self.data),
                      len(skipped_due_to_zero)))
        for s in skipped_due_to_zero:
            print(s)

        print('\n'
              'Done!'
              '\n--------------------------------------------------')
        # return self.data

    def filter_date(self, *args, **kwargs):
        """
        ARGS:
            pass a datestring (2017-05-01), tuple ((2017,05,01))
        KWARGS:
            from:
            to:

        """
        def create_dt(date):
            if isinstance(date, str):
                date = (date.replace('-', ' ').replace(',', ' ')
                        .replace('_', ' ').replace('/', ' ').split())
                year, month, day = map(int, date)
                return datetime.date(year, month, day)

            if isinstance(date, tuple):
                print(date)
                year, month, day = map(int, date)
                return datetime.date(year, month, day)

        if args:  # If args is not empty.
            if len(args) == 1:
                date = create_dt(args[0])
                if len(self.data[self.data.DATE.dt.date == date]) > 0:
                    return self.data[self.data.DATE.dt.date == date]
                else:
                    delta = self.data.DATE.dt.date - date
                    delta_min = min(delta, key=abs)
                    nearest = list(self.data.DATE.dt.date[
                        self.data.DATE.dt.date - date == delta_min])[0]
                    # indexmax = (diff[(diff < pd.to_timedelta(0))].idxmax())
                    print('nothing found for date \'{}\'.'
                          '\n Nearest date is \'{}\'.'
                          .format(date, nearest))
            else:
                raise ValueError('Too many dates specified!\n'
                                 'Use either string/tuple for single date or'
                                 'keyword arguments \'start=\', \'end=\''
                                 'for daterange.')
        else:
            if kwargs:
                self.start = create_dt(kwargs.get('start', None))
                self.end = create_dt(kwargs.get('end', None))
                print('start: {}, end: {}'.format(self.start, self.end))
                return self.data[(self.data.DATE.dt.date >= self.start) &
                                 (self.data.DATE.dt.date <= self.end)]
            else:
                raise ValueError('No date specified!')

    def filter_field(self, fieldnumber):
        """
        pass fieldnumber (e.g. 542) or ESU_ID (542_medium)
        """
        return self.data[self.data.loc[:, 'ESU_ID'].
                         str.contains(str(fieldnumber).replace(' ', '_'))]


def main():
    # try:
    #     input = raw_input
    # except NameError:
    #     pass

    orig_wd = os.getcwd()
    count = 0
    print("\nWanna use current working directory \n", orig_wd,
          "\n to extract LAI values? Enter \"n\" to enter alternative path.")
    do = input("\n[Y/n] ")
    if (do == "y" or do == "" or do == "Y"):
        path = orig_wd
    elif do == "n":
        while True:
            path = input("Please enter path to .TXT LAI-files: "
                         "[e.g. /media/thomas/Work/Data/LAI]"
                         "\n -->   ")
            if os.path.exists(path):
                break
            elif count > 2:
                raise Exception("entered invalid path too many times.")
            else:
                count += 1
                print("Entered path does not exist.  Try again...")
    else:
        raise Exception("User interruption.")
    os.chdir(path)
    # os.chdir('/media/thomas/Work/Data/LAI_DATA/')
    LAI = LAIdata()
    LAI.extract_LAI()
    LAI.save_LAIdata()


if __name__ == "__main__":
    main()
