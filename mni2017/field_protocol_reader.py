#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This script helps extracting LAI values from txt files.
"""
# from __future__ import division, print_function, absolute_import, iteritems
# import __future__
# from xlrd import open_workbook
import re
import os
import pandas as pd
import numpy as np
# from scipy.spatial import cKDTree
import datetime
from matplotlib import pyplot as plt
import matplotlib.lines as mlines
import matplotlib.dates as mdates

# from mni2017 import __version__

__author__ = "Thomas Ramsauer"
__copyright__ = "Thomas Ramsauer"
__license__ = "gpl3"


class field_data(object):

    def __init__(self, filename='MULTIPLY.xlsx',
                 #  create_df=True
                 ):

        self.data = self._read_all_xlsx(filename)
        self._clean_input()
        # if create_df is True:
        #     self.data = create_reasonable_multiindex_df(self.data.items())
    #
    # def __getitem__(self, key):
    #     return self.data
    #
    # def __str__(self):
    #     return '{}'.format(self.data)

    def _read_all_xlsx(self, filename):
        try:
            data = pd.read_excel(filename, sheetname=None,
                                 header=[1], index_col=0,
                                 convert_float=False, decimal=",")
        except IOError as e:
            print('No data loaded [{}]'.format(e))
        return data

    def _clean_input(self):
        for key in list(self.data.keys()):
            # drop all lines containing only NaNs + drop rows indexed Nan/None
            self.data[key] = self.data[key].dropna(how='all').drop(None)
            # Update keys to be a datetime.date object
            self.data[datetime.datetime.strptime(str(key)
                                                 .replace(".", ""), "%d%m%Y").date()] = self.data.pop(key)
        # more cleanup?

    def add_xlsx(self, filename):
        """
        Add an xlsx file to the existing class object.
        """
        new_data = pd.read_excel(filename, sheetname=None,
                                 header=[1], index_col=0,
                                 convert_float=False, decimal=",")
        if isinstance(new_data, dict):
            self.data = self.data.update(new_data)
        else:
            print('only a single sheet to import? not implemented yet..')

    def show_time(self):
        print(self.data.keys())

    def filter_date(self, date=None, *args, **kwargs):
        """
        ARGS:
            pass a datestring (2017-05-01), tuple ((2017,05,01))
        KWARGS:
            from:
            to:

        """

        def extract_date_data(date):
            if (date in self.data.keys()):
                return self.data[date]
            else:  # print nearest date
                delta_min = datetime.timedelta(9999)
                key_min = None
                for key in self.data.keys():
                    delta = abs(key - date)
                    if delta < delta_min:
                        delta_min = delta
                        key_min = key
                    elif delta == delta_min:
                        # print('old key_min: {}'.format(key_min))
                        # print('new key: {}'.format(key))
                        key_min = [key_min]
                        key_min.append(key)
                        # print('new key_min: {}'.format(key_min))
                    else:
                        continue
                if delta_min == datetime.timedelta(9999):
                    print('no suitable dates found.')
                else:
                    print('nothing found for date \'{}\'.'
                          '\n Nearest date is \'{}\'.'
                          .format(date, key_min))

        if date is not None:
            date = self._create_dt(date)
            return extract_date_data(date)

        if args:  # If args is not empty.
            if len(args) == 1:
                date = self._create_dt(args[0])
                # print('date is: {}'.format(date))
                return extract_date_data(date)
            else:
                raise ValueError('Too many dates specified!\n'
                                 'Use either string/tuple for single date or'
                                 'keyword arguments \'start=\', \'end=\''
                                 'for daterange.')
        else:
            if kwargs:
                self.start = self._create_dt(kwargs.get('start', None))
                self.end = self._create_dt(kwargs.get('end', None))
                print('start: {}, end: {}'.format(self.start, self.end))
                export_keys = []
                return_data = {}
                for key in self.data.keys():
                    if key <= self.end and key >= self.start:
                        print('suitable: {}'.format(key))
                        return_data[key] = []
                        return_data[key] = self.data[key]
                        export_keys.append(key)
                return return_data
            else:
                raise ValueError('No date specified!')

    def filter_field(self, fieldnumber):
        """
        Filter by Fieldnumber (e.g. 542) or ESU_ID (542_medium)

        INPUT:
        -------
        fieldnumber: int, str
            Integer or string reprsenting the field to extract


        RETURNS:
        --------
        Dictionary with values populated by dataframes only containing columns
            that match fieldnumber string/integer.
        """

        return_data = {}
        fieldnumber_str = str(fieldnumber)
        # fieldnumber_str = re.sub('[^0-9a-zA-Z ]+', '', fieldnumber_str)
        field_id = re.findall(r'(?:[0-9]+)?(?:[a-z]+)?', fieldnumber_str, re.I)
        field_id = list(filter(None, field_id))  # strip empty entries in list
        # print(field_id)
        # print(len(field_id))
        return_data = {}

        for key, value in self.data.items():
            # print('key is: {}'.format(key))

            if len(field_id) == 2:
                col_id = (value.columns.str.contains(field_id[0]) &
                          value.columns.str.contains(field_id[1]))
            if len(field_id) == 1:
                col_id = (value.columns.str.contains(field_id[0]))
            return_data[key] = value.loc[:, col_id]
            ncol = sum(col_id)
            print(('[filter_field]: Extracting {} field columns for {}.'
                   .format(ncol, key)))
        return return_data

    def filter(self, fieldnumber=None, values=[], date=None, *args, **kwargs):
        """
        TBD

        """
        orig_self_data = self.data

        if date is not None or args or kwargs:
            self.data = self.filter_date(date, *args, **kwargs)
            # print('After date: {}'.format(self.data))
        if fieldnumber is not None:
            if isinstance(self.data, pd.DataFrame):
                self.data = {self._create_dt(date): self.data}
            self.data = self.filter_field(fieldnumber)
            # print('After field: {}'.format(self.data))

            if len(values) > 0:
                # print('Before values: {}'.format(self.data))
                temp_data = [{s: (pd.DataFrame(self.data[s].loc[values, :]))}
                             for s in self.data]
                self.data = {}
                for i in temp_data:
                    self.data.update(i)
                # print('After values: {}'.format(self.data))

        if len(values) > 0 and fieldnumber is None:
            print('so far fieldnumber has to be specified to extract single'
                  'values..')

        return_data = self.data
        if isinstance(return_data, dict):
            # print(return_data)
            return_data = [tuple(t) for t in return_data.items()]
            # print(return_data)
            return_data.sort(key=lambda tup: tup[0])
        if isinstance(return_data, list):
            # print(return_data)
            return_data.sort(key=lambda tup: tup[0])
            # print(['warning return data not sorted. no instance of list.'])
        self.data = orig_self_data
        return return_data

    def _create_dt(self, date):
        if isinstance(date, str):
            date = (date.replace('-', ' ').replace(',', ' ')
                    .replace('_', ' ').replace('/', ' ').split())
            year, month, day = map(int, date)
            return datetime.date(year, month, day)

        if isinstance(date, tuple):
            print(date)
            year, month, day = map(int, date)
            return datetime.date(year, month, day)


def create_multiindex_df(data):
    dic = {}
    for outerKey, Dataframe in data:
        for colname in list(Dataframe):
            for rowname in list(Dataframe.index):
                dic.update({(outerKey, colname, rowname):
                            [Dataframe.ix[rowname, colname]]})

    return pd.DataFrame(dic)


def create_reasonable_multiindex_df(data):
    dic = {}
    index_list = []
    for outerKey, Dataframe in data:
        for colname in list(Dataframe):
            for rowname in list(Dataframe.index):
                dic.update({(outerKey, colname, rowname):
                            [Dataframe.ix[rowname, colname]]})
                index_list.append(outerKey)
    return_df = pd.DataFrame(dic).stack(0)
    return_df.index = return_df.index.droplevel(0)
    return return_df


def plot_field(data, title=None, legend=False, save=False):
    """
    INPUT:
        fitered field_data class object (self.filter with 'fieldnumber' and
        'value(s)' specified).

        e.g.
        data = field.filter(fieldnumber='301_med',
                            values=['BBCH', 'Height [cm]'])

    RETURNS:
        prints data
    """
    x, y = (zip(*data))
    y
    # y[1].iloc[1]
    k = []
    names = list(y[0].index)
    [k.append([]) for i in range(len(names))]
    k
    if isinstance(y[0], pd.DataFrame):
        for i in range(len(y)):
            for j in range(len(y[i])):
                v = (y[i].iloc[j]).values.tolist()[0]
                try:
                    k[j].append(float(v.replace(',', '.')))
                except:
                    k[j].append(v)

    fig = plt.figure()
    ax = fig.add_subplot(111)

    colormap = plt.get_cmap('Set1')
    colors = [colormap(i) for i in np.linspace(0, 1, len(k))]
    lines, axes, handles_all, labels_all = [], [], [], []
    [lines.append("l" + str(i)) for i in range(len(k))]
    [axes.append("ax" + str(i)) for i in range(len(k))]
    lines
    axes

    for i, ln in enumerate(lines):

        ax_old = ax  # to get new ids

        if i > 0:
            axis = ax_old.twinx()
            axis.spines['right'].set_color(colors[i])
            axis.spines['right'].set_position(('outward', (2 + (i - 1) * 40)))
        else:
            axis = ax
            axis.spines['left'].set_color(colors[i])
            axis.spines['left'].set_position(('outward', 2))
        axis.tick_params(axis='y', colors=colors[i])
        axis.plot(x, k[i], label=names[i], color=colors[i], marker="*")
        axis.set_ylabel(names[i], color=colors[i])
        axis.tick_params(axis='y', color=colors[i])

        # Gather data for legend
        labels = names[i]
        labels_all.append(labels)

        handles = mlines.Line2D([], [], color=colors[i], label=names[i])
        handles_all.append(handles)
    if legend is True:
        lgd = ax.legend(handles_all, labels_all,
                        # bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                        bbox_to_anchor=(0., -0.3, 1, .102), loc='upper left',
                        mode="expand", borderaxespad=0.)

    ax.grid()
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%Y'))
    plt.gcf().autofmt_xdate()
    fig.tight_layout()
    if title is not None:
        plt.title(title)
    if save is True:
        if title is not None:
            fig_name = '{}.png'.format(''.join(e for e in title if
                                               (e.isalnum()or
                                                e == "_" or
                                                e == "-")))
        else:
            fig_name = ('plot_' +
                        datetime.datetime.now().strftime('%Y%m%d_%H%M') +
                        '.png')
        if legend is True:
            fig.savefig(fig_name, bbox_extra_artists=(lgd,),
                        bbox_inches='tight', dpi=300)
        else:
            fig.savefig(fig_name, bbox_inches='tight',
                        dpi=300)
    plt.show()


def plot_from_reasonable_df(df, field_id, values, title=None,
                            legend=False, save=False):
    """
    df: dataframe object
        reasonable df (Multiindex Df with field, values as column indices and
        dates as row indices)
    field_id: string
        field name in column names ('301 (WA) medium')
    values: list of stringsidasdfasdfasdfa
        list of values to plot (e.g. ['BBCH'])
    title/legend: bool
        Should the plot contain a title/legend?
    """

    names = [str(s) for s in values]
    k = values

    daframe = df[field_id]
    # y1 = daframe.apply(lambda x: pd.to_numeric(x.astype(str)
    #                              .str.replace(',',''), errors='coerce'))
    # y1 = daframe.apply(lambda x: pd.to_numeric(x.str.replace(',', '.'),
    #                                            errors='coerce'))
    y = daframe[values].T.values.tolist()
    x = df.index

    fig = plt.figure()
    ax = fig.add_subplot(111)

    colormap = plt.get_cmap('Set1')
    colors = [colormap(i) for i in np.linspace(0, 1, len(k))]
    lines, axes, handles_all, labels_all = [], [], [], []
    [lines.append("l" + str(i)) for i in range(len(k))]
    [axes.append("ax" + str(i)) for i in range(len(k))]
    lines
    axes

    for i, ln in enumerate(lines):

        ax_old = ax  # to get new ids

        if i > 0:
            axis = ax_old.twinx()
            axis.spines['right'].set_color(colors[i])
            axis.spines['right'].set_position(('outward', (2 + (i - 1) * 40)))
        else:
            axis = ax
            axis.spines['left'].set_color(colors[i])
            axis.spines['left'].set_position(('outward', 2))
        axis.tick_params(axis='y', colors=colors[i])
        axis.plot(x, y[i], label=names[i], color=colors[i], marker="*")
        if len(names[i]) < 25:
            axis.set_ylabel(names[i], color=colors[i])
        else:
            axis.set_ylabel(names[i][:26] + '...', color=colors[i])
        axis.tick_params(axis='y', color=colors[i])

        # Gather data for legend
        labels = names[i]
        labels_all.append(labels)

        handles = mlines.Line2D([], [], color=colors[i], label=names[i])
        handles_all.append(handles)
    if legend is True:
        lgd = ax.legend(handles_all, labels_all,
                        # bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                        bbox_to_anchor=(0., -0.3, 1, .102), loc='upper left',
                        mode="expand", borderaxespad=0.)

    ax.grid()
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%Y'))
    plt.gcf().autofmt_xdate()
    fig.tight_layout()
    if title is not None:
        plt.title(title)
    if save is True:
        if title is not None:
            fig_name = '{}.png'.format(''.join(e for e in title if
                                               (e.isalnum()or
                                                e == "_" or
                                                e == "-")))
        else:
            fig_name = ('plot_' +
                        datetime.datetime.now().strftime('%Y%m%d_%H%M') +
                        '.png')
        if legend is True:
            fig.savefig(fig_name, bbox_extra_artists=(lgd,),
                        bbox_inches='tight', dpi=300)
        else:
            fig.savefig(fig_name, bbox_inches='tight',
                        dpi=300)
    plt.show()


def run():

    # path = '/media/thomas/NAS_Data/FINISHED_PROJECTS/2017_MNI_Campaign/'
    path = '/media/thomas/Work/Data/MNI_Vortrag/'
    os.chdir(path)
    field = field_data(filename='MULTIPLY.xlsx')
    field_df = create_reasonable_multiindex_df(field.data.items())
    field_df[:2]
    list(field_df['301 (WT) high'])
    fieldnames = list(set(field_df.columns.get_level_values(0)))
    fieldnames.sort()
    fieldnames
    fieldnames = fieldnames[0:3] + fieldnames[6:9] + fieldnames[12:15]
    # plot_from_reasonable_df(field_df, field_id='301 (WT) high',
    #                         values=['Water content leaf [%]',
    #                         'Water content stem [%]'])

    fig = plt.figure()
    ax = fig.add_subplot(111)

    colormaps = ['Greens', 'Purples', 'Blues', 'Oranges', 'Reds']
    j = 0

    for i, fieldname in enumerate(fieldnames):

        if i % 3 == 0:
            print(i, j)
            colormap = plt.get_cmap(colormaps[j])
            colors = [colormap(k) for k in np.linspace(0.5, 0.9, 3)]
            # colors[::-1]
            j = j + 1
            l = 0
        ax.plot(field_df[fieldname]['Water content total [%]'],
                label=fieldname, color=colors[l], marker="*")
        l = l + 1
    ax.grid()
    # ax.set_ylim(60,90)
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%Y'))
    plt.gcf().autofmt_xdate()
    fig.tight_layout()
    ax.legend()
    plt.show()

    # field_0504 = field.filter_date(start='2017,03,26', end='2017,04,06')
    # type(field_0504)
    # # print(field_0504)
    #
    # field_0504 = field.filter_date('2017,04,05')
    # type(field_0504)
    # print(field_0504)

    # field_301 = field.filter_field('301_med')

    # field_301_28032017 = field.filter(fieldnumber='301 ', date='2017, 03,28',
    #                                   values=['BBCH'])
    # field_301_28032017
    # field_301_28032017[0].keys()
    # field_301_28032017[0][field_301_28032017[0].keys()[0]]
    # #
    # t = field.filter(fieldnumber='301', date='2017, 03,28')
    # t
    # t[t.keys()[0]]

    # t = field.filter(fieldnumber='301_med')

    # t = field.filter(fieldnumber='301_med',
    #                  values=['BBCH', 'Height [cm]',
    #                          ('Chlorophyll measurement 1'
    #                           ' [avarage of 10 SPAD Units]')])

    # t = field.filter(fieldnumber='301_med',
    #                  values=['Chlorophyll measurement 1'
    #                          ' [avarage of 10 SPAD Units]',
    #                          'Chlorophyll measurement 2'
    #                          ' [avarage of 10 SPAD Units]',
    #                          'Chlorophyll measurement 3'
    #                          ' [avarage of 10 SPAD Units]',
    #                          'Chlorophyll measurement 4'
    #                          ' [avarage of 10 SPAD Units]',
    #                          'Chlorophyll measurement 5'
    #                          ' [avarage of 10 SPAD Units]'])

    # t
    # # t.sort(key=lambda tup: tup[1])
    # print(t[0])
    #
    # print(t[0][1].iloc[0])
    # print(float(t[1][1].iloc[0]))
    #
    # plot_field(t, title='301_med', save=True)
    # pass


if __name__ == "__main__":
    run()
